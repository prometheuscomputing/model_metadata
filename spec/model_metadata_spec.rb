require 'spec_helper'

describe 'model_metadata functionality' do
  it 'should provide metainformation describing interface impelmentation' do
    Person.interfaces.should =~ [Named, NamedAndIdentified]
    Named.implementors.should =~ [Person, Clown, Mime]
    NamedAndIdentified.implementors.should =~ [Person, Clown, Mime]
  end
  
  it 'should correct propagate class properties to subclasses' do
    # Interface property
    Named.interface?.should be true
    NamedAndIdentified.interface?.should be true
    # Enumeration property
    Color.enumeration?.should be true
    SimpleColor.enumeration?.should be true
    # Association class property
    MyAssociationClass.association_class?.should be true
    MyAssociationSubClass.association_class?.should be true
    # Singleton property
    MySingletonClass.singleton?.should be true
    MySingletonSubClass.singleton?.should be true
  end
  
  it "should be able to return a list of inherited properties" do
    Person.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier]
    Clown.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    NamedAndIdentified.properties.keys.should =~ [:name, :identifier]
    Color.properties.keys.should == []
  end
  
  it "should invalidate the properties cache for subclasses when a parent class's properties are modified" do
    Person.add_immediate_property(:foobar, {})
    Clown.properties.keys.should include(:foobar)
    # Remove property
    Person.remove_immediate_property(:foobar)
    Clown.properties.keys.should_not include(:foobar)
  end
  
  it "should invalidate the properties cache if inheritance or an interface implementation is added" do
    Mimic.properties.keys.should =~ [:size]

    # Make Mimic inherit from Entity
    Mimic.child_of Entity
    Mimic.properties.keys.should =~ [:size, :lifespan]

    # Make Mimic inherit from Person
    Mimic.child_of Person
    Mimic.properties.keys.should =~ [:size, :lifespan, :phone_number, :name, :identifier]
    
    # Make Mimic implement CopyOf
    Mimic.implements CopyOf
    Mimic.properties.keys.should =~ [:size, :lifespan, :phone_number, :name, :identifier, :base]
  end
  
  it "should be able to handle creating and removing local modifications to inherited properties" do
    # Set testable property for :name
    Person.add_immediate_property(:name, {:foo => :bar})
    # Test that attribute keys remain unchangeed
    Person.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier]
    Clown.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    Mime.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    # Test :name on Person
    Person.properties[:name].should == {:foo => :bar}
    # Test :name on Clown
    Clown.properties[:name].should == {:foo => :bar}
    # Test :name on Mime
    Mime.properties[:name].should == {:foo => :bar}
    
    # Locally modify :name on Clown
    Clown.add_immediate_property(:name, {:foo => :baz})
    # Test that attribute keys remain unchangeed
    Person.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier]
    Clown.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    Mime.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    # Test :name on Person
    Person.properties[:name].should == {:foo => :bar}
    # Test that :name on Clown has been altered
    Clown.properties[:name].should == {:foo => :baz}
    # Test that :name on Mime has been altered
    Mime.properties[:name].should == {:foo => :baz}

    # Remove :name modifications from Clown
    Clown.remove_immediate_property(:name)
    # Test that attribute keys remain unchangeed
    Person.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier]
    Clown.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    Mime.properties.keys.should =~ [:phone_number, :lifespan, :name, :identifier, :shoe_size]
    # Test :name on Person
    Person.properties[:name].should == {:foo => :bar}
    # Test :name on Clown
    Clown.properties[:name].should == {:foo => :bar}
    # Test :name on Mime
    Mime.properties[:name].should == {:foo => :bar}
  end
  
  it "should not be super slow!" do
    before_time = Time.now
    300000.times do
      Mime.properties
    end
    time_taken = Time.now - before_time
    puts "Took: #{time_taken} seconds"
    # This takes 0.16 seconds on my machine, so if it takes more than 1s, something is probably wrong -SD
    expect(time_taken).to be < 1
  end
end
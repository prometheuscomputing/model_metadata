require 'rspec'

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

require 'simplecov'
SimpleCov.start do
  add_filter 'spec/'
  # add_filter 'tmp/'
end

$:.unshift(File.dirname(__FILE__) + '/../../alternate_inheritance/lib')
require_relative '../lib/model_metadata'
require_relative 'metadata_example'

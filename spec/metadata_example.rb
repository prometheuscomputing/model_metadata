# Interface test
class Named
  extend ModelMetadata
  interface!
  attr_accessor :name
  add_immediate_property(:name, {})
end
class NamedAndIdentified < Named
  attr_accessor :identifier
  add_immediate_property(:identifier, {})
end
class Entity
  extend ModelMetadata
  attr_accessor :lifespan
  abstract!
  add_immediate_property(:lifespan, {})
end
class Person < Entity
  implements NamedAndIdentified
  attr_accessor :phone_number
  add_immediate_property(:phone_number, {})
end
class ClownTraits
  extend ModelMetadata
  interface!
  attr_accessor :shoe_size
  add_immediate_property(:shoe_size, {})
end
class Clown < Person
  implements ClownTraits
end
class Mime < Clown
end
class Mimic
  extend ModelMetadata
  attr_accessor :size
  add_immediate_property(:size, {})
end
class CopyOf
  extend ModelMetadata
  interface!
  attr_accessor :base
  add_immediate_property(:base, {})
end

# Enumeration test
class Color
  extend ModelMetadata
  enumeration!
end
class SimpleColor < Color
end

# Association class test
class MyAssociationClass
  extend ModelMetadata
  association_class!
end
class MyAssociationSubClass < MyAssociationClass
end

# Singleton test
class MySingletonClass
  extend ModelMetadata
  # This isn't how a real singleton should work.
  # TODO: fix this when writing singleton testing.
  def self.first; self.new; end
  def self.create; self.new; end
  def self.instantiate; self.new; end
  singleton!
end
class MySingletonSubClass < MySingletonClass
end
module ModuleDocumentation
  def documentation(str = nil)
    if str.nil?
      @documentation
    else
      @documentation = str
    end
  end
end
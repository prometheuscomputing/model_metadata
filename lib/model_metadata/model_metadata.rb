require 'alternate_inheritance'
require 'alternate_inheritance/interfaces'
module ModelMetadata
  extend self
  include AlternateInheritance::InterfaceImplementor

  def extended extender
    AlternateInheritance::InterfaceImplementor.extended extender
  end
  
  # Hook inheritance to propagate class properties
  def on_parent_addition parent_class
    association_class! if parent_class.association_class?
    singleton! if parent_class.singleton?
    enumeration! if parent_class.enumeration?
    interface! if parent_class.interface?
    super
    invalidate_properties_cache
  end
  
  def on_interface_addition interface
    super
    self.invalidate_properties_cache
  end

  # data is a hash
  def additional_data(data = nil)
    if data.nil?
      @additional_data
    else
      @additional_data = data
    end
  end

  def documentation(str = nil)
    if str.nil?
      @documentation
    else
      @documentation = str
    end
  end
  
  # Define association class meta-information -- Call association_class! to mark a class as abstract
  def association_class!
    @association_class = true
  end
  def association_class?
    @association_class == true
  end
  
  # Define singleton class meta-information -- Call singleton! to mark a class as a singleton
  def singleton!
    return if singleton?
    # Make normal creation methods private
    private_class_method :new, :create, :instantiate
    # def instance
    define_singleton_method(:instance) do
      self.first || self.send(:create)
    end
    @singleton = true
  end
  def singleton?
    @singleton == true
  end
  
  # Define abstract meta-information -- Call abstract! to mark a class as abstract
  def abstract!
    @abstract = true
  end
  def abstract?
    @abstract == true
  end
  
  def enumeration!
    @enumeration = true
  end
  def enumeration?
    @enumeration == true
  end
  
  def interface!
    extend AlternateInheritance::Interface
  end
  def interface?
    @interface == true
  end
  
  def primitive?
    base_prim_classes = [String, Numeric, TrueClass, FalseClass, Time, NilClass, Regexp]
    base_prim_classes << Date if defined?(Date)
    (ancestors & base_prim_classes).any?
  end
  
  def attributes(include_derived = true)
    properties(include_derived).select{|name, info| info[:attribute]}
  end
  def associations(include_derived = true)
    properties(include_derived).select{|name, info| info[:association]}
  end
  
  def properties(include_derived = true)
    props = get_property 'properties'
    props = props.reject{|getter, info| info[:derived]} unless include_derived
    props

    # historical information....
    # attributes uses the following keys as of 3-13-2015: [:column, :complex, :methods, :name, :store_on_get, :store_value, :type]
    # associations uses the following keys as of 3-13-2015: [:as, :class, :composition, :enumeration, :for_complex_attribute, :getter, :holds_key, :methods, :opp_getter, :opp_is_ordered, :ordered, :singular_getter, :singular_opp_getter, :through, :type]
    # old code that mapped attributes hash values into the same API as the associations hash
    # new_attrs = attributes.dup
    # new_attrs.each do |k,v|
    #   v[:class] = v.delete :type
    #   v[:getter] = v.delete :name
    #   v[:type] = (v[:complex] ? :complex_attribute : :attribute)
    # end
    # associations.merge new_attrs
  end
  
  # Invalidate immediate properties hash for this classifier only
  def invalidate_immediate_properties_cache
    set_cache_for_property('properties', nil)
  end
  
  # Invalidate properties cache for this class and all classes that inherit from it
  def invalidate_properties_cache
    downstream_classifiers = [self] + children
    if self.interface?
      downstream_classifiers += downstream_classifiers.collect{|int| int.implementors}
    end
    downstream_classifiers.flatten.uniq.each do |klass|
      klass.invalidate_immediate_properties_cache
    end
  end
  
  # get only those properties that are defined directly on this class (do not include inherited properties)
  def immediate_properties
    @properties ||= {}
  end
  
  def add_immediate_property(name, info)
    immediate_properties[name] = info
    invalidate_properties_cache
    info
  end
  
  # Remove a property from the immediate properties hash
  # This will affect any modifications to inherited properties in this class or subclasses, but will not affect inherited properties
  def remove_immediate_property(name)
    removed = immediate_properties.delete(name)
    invalidate_properties_cache
    removed
  end
  
  def inherited_properties
    ipk = immediate_properties.keys
    properties(false).reject{|k,v| ipk.include? k}
  end
  
  def compositions(include_derived = true)
    self.properties(include_derived).select{|getter, info| info[:composition]}.values
  end
  def composition_getters(include_derived = true)
    compositions(include_derived).collect{|c| c[:getter]}
  end
  
  def aggregations(include_derived = true)
    self.properties(include_derived).select{|getter, info| info[:composition] || info[:shared]}.values
  end
  def aggregation_getters(include_derived = true)
    aggregations(include_derived).collect{|c| c[:getter]}
  end
  # --------------------------------------------
  
  # this searches through all of the properties going up the inheritance chain
  # Intialize properties to empty hash
  # 
  # This is misleadingly named. This is a meta method used by model_metadata to construct inherited hashes such as #properties
  # It should not be used outside model_metadata -SD
  # 
  def get_property(property_name)
    cache = get_cache_for_property(property_name)
    return cache if cache
    properties = {}
    # Breadth first traversal of parents so closer properties take precedence
    ([self] + parents(:traversal => :breadth)).each do |klass|
      next unless klass.respond_to?(property_name)
      # Get 'immediate' properties from this parent
      klass_properties = klass.send("immediate_#{property_name}")
      klass.immediate_interfaces.each do |klass_interface|
        next unless klass_interface.respond_to?(property_name)
        klass_properties = klass_interface.send(property_name).merge(klass_properties)
      end
      properties = klass_properties.merge(properties)
    end
    set_cache_for_property(property_name, properties)
    properties
  end
  
  def get_cache_for_property(property_name)
    self.instance_variable_get("@cached_#{property_name}")
  end
  
  def set_cache_for_property(property_name, cache)
    self.instance_variable_set("@cached_#{property_name}", cache)
  end
end # ModelMetaData

# WARNING: This file seems to be untested and it is unclear what its purpose is. -SD

module ModelMetadata
  BASE_PRIMITIVES = [String, Numeric, TrueClass, FalseClass, Time, NilClass, Regexp]
  BASE_PRIMITIVES << Date if defined?(Date)
  module PrimtiveExtensions
    def additional_data(data = nil)
      if data.nil?
        @additional_data
      else
        @additional_data = data
      end
    end

    def documentation(str = nil)
      if str.nil?
        @documentation
      else
        @documentation = str
      end
    end
  
    def association_class!
      raise "#{self} can not be an association class"
    end
    def association_class?
      false
    end
  
    # Define singleton class meta-information -- Call singleton! to mark a class as a singleton
    def singleton!
      raise "#{self} can not be a singleton class"
    end
    def singleton?
      false
    end
  
    # FIXME maybe it is technically possible to define an abstract primitive class
    def abstract!
      raise "#{self} can not be an abstract class"
    end
    def abstract?
      false
    end
  
    def enumeration!
      raise "#{self} can not be an enumeration class"
    end
    def enumeration?
      false
    end
  
    def interface!
      raise "#{self} can not be an interface"
    end
    def interface?
      false
    end
  
    def primitive?
      true
      # base_prim_classes = [String, Numeric, TrueClass, FalseClass, Time, NilClass, Regexp]
      # base_prim_classes << Date if defined?(Date)
      # (ancestors & base_prim_classes).any?
    end
  
    def attributes(include_derived = true)
      properties(include_derived).select{|name, info| info[:attribute]}
    end
    def associations(include_derived = true)
      properties(include_derived).select{|name, info| info[:association]}
    end
  
    def properties(include_derived = true)
      props = get_property 'properties'
      props = props.reject{|getter, info| info[:derived]} unless include_derived
      props
    end
  
    # get only those properties that are defined directly on this class (do not include inherited properties)
    def immediate_properties; {}; end
      # @properties ||= {}
    # end
    
    # A primitive extension seems to not be able to have immediate properties, so this method will raise if called. -SD
    def add_immediate_property(name, info)
      raise "You cannot add immediate properties to primitive classes"
    end
  
    def inherited_properties
      ipk = immediate_properties.keys
      properties(false).reject{|k,v| ipk.include? k}
    end
  
    def compositions(include_derived = true)
      self.properties(include_derived).select{|getter, info| info[:composition]}.values
    end
    def composition_getters(include_derived = true)
      compositions(include_derived).collect{|c| c[:getter]}
    end
  
    # --------------------------------------------
  
    # this searches through all of the properties going up the inheritance chain
    # Intialize properties to empty hash
    #
    # I don't understand this. Why does #properties appear to retrieve a list of inherited properties, but #get_property always returns empty hash? -SD
    #
    def get_property(property_name); {}; end
    #   properties = {}
    #   # Breadth first traversal of parents so closer properties take precedence
    #   ([self] + parents(:traversal => :breadth)).each do |klass|
    #     next unless klass.respond_to?(property_name)
    #     # Get 'immediate' properties from this parent
    #     # TODO: make immediate properties a separate method 'immediate_<property_name>'
    #     klass_properties = klass.instance_variable_get("@#{property_name}")
    #     next unless klass_properties.is_a?(Hash) # #associations will return an empty array for Sequel::Model
    #     klass.immediate_interfaces.each do |klass_interface|
    #       next unless klass_interface.respond_to?(property_name)
    #       klass_properties = klass_interface.send(property_name).merge(klass_properties)
    #     end
    #     properties = klass_properties.merge(properties)
    #   end
    #   properties
    # end
  end # PrimtiveExtensions  
end # ModelMetadata
  
ModelMetadata::BASE_PRIMITIVES.each do |klass|
  klass.class_eval {extend ModelMetadata::PrimtiveExtensions}
end